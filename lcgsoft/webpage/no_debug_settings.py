# no_debug_settings.py

# pull in the normal settings
from settings import *
import os

# no debug for us
DEBUG = False
COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True

ALLOWED_HOSTS = [
    "127.0.0.1",
    "localhost",
    'lcgapp-centos7-lcgsoft',
    '188.185.68.150',
    'lcgapp-centos7-lcgsoft.cern.ch',
    'lcginfo.cern.ch'
]

memcached_ip = '127.0.0.1:11211'
if os.environ.has_key('MEMCACHED_IP'):
    memcached_ip = os.environ['MEMCACHED_IP']
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': memcached_ip,
    }
}
