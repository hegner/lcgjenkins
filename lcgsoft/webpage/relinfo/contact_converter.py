#!/usr/bin/python
# coding=utf-8

debug = False

def convert_contact(contact):
    # type: (str) -> list
    """ Covert contact information from the old packageinfo.txt file to the new contacts format used in Django. """
    
    new_contacts = list()
    
    # Split separators
    if ' and ' in contact:
        prepared = contact.split(' and ')
    elif ', ' in contact:
        prepared = contact.split(', ')
    elif '; ' in contact:
        prepared = contact.split('; ')
    else:
        prepared = [ contact ]
    
    if debug:
        print prepared
    
    # Split words
    for p in prepared:
        
        words = p.split()
        len_before = len(new_contacts)
        
        for i, elem in enumerate(words):
            # Find out websites
            if '://' in elem:
                new_contact = { "name": unicode(elem.strip()), "email": unicode(elem.strip()) }
                new_contacts.append(new_contact)
                break
            elif '@' in elem:
                if i == 0:
                    email = elem
                    if len(words) == 1:
                        # Email only
                        name = elem
                    else:
                        # Email first, name last
                        name = ' '.join(words[i + 1:])
                elif i == len(words) - 1:
                    # Name first, email last
                    name = ' '.join(words[:i])
                    email = elem
                else:
                    print 'ERROR. Unknown contact format: %s' % prepared
                
                # Strip whitespaces
                new_contact = { "name": unicode(name.strip()), "email": unicode(email.strip()) }
                new_contacts.append(new_contact)
                break
                
        if len(new_contacts) == len_before:
            name = ' '.join(words).strip()
            if name != '' and name.lower() != 'none':
                # Only name; add name also as email because it is a unique property
                new_contact = { "name": unicode(name), "email": unicode(name) }
                new_contacts.append(new_contact)
    if debug:
        print 'Converted contact "%s" to %d new contacts: "%s"' % (contact, len(new_contacts), new_contacts)
    return new_contacts


if __name__ == '__main__':
    
    debug = True
    
    # Empty string
    convert_contact("")
    print "-" * 75
    
    # None
    convert_contact("None")
    print "-" * 75
    
    # Only email
    convert_contact("clhep-editors@cern.ch")
    convert_contact("automake@gnu.org")
    print "-" * 75
    
    # Only name
    convert_contact("Gabi Melman")
    convert_contact("Google")
    print "-" * 75
    
    # Name and email
    convert_contact("Baptiste Barthelemy Lepilleur blep@users.sourceforge.net")
    convert_contact("Baptiste Lepilleur blep@users.sourceforge.net")
    convert_contact("Lepilleur blep@users.sourceforge.net")
    print "-" * 75
    
    # Email and name
    convert_contact("blep@users.sourceforge.net Baptiste Barthelemy Lepilleur")
    convert_contact("blep@users.sourceforge.net Baptiste Lepilleur")
    convert_contact("blep@users.sourceforge.net Lepilleur")
    print "-" * 75
    
    # Name with three parts
    convert_contact("Carlos Martin Nieto cmn@dwim.me")
    print "-" * 75
    
    # Leading whitespace
    convert_contact(" Ansgar Denner")
    convert_contact(" collier@projects.hepforge.org")
    convert_contact(" Ansgar Denner collier@projects.hepforge.org")
    print "-" * 75
    
    # Trailing whitespace
    convert_contact("Ansgar Denner ")
    convert_contact("collier@projects.hepforge.org ")
    convert_contact("Ansgar Denner collier@projects.hepforge.org ")
    print "-" * 75
    
    # Separator 'and'
    convert_contact("Gary V. Vaughan gary@gnu.org and Eric Blake ebb9@byu.net")
    convert_contact("Hans Staudenmaier and Hans Kuehn")
    convert_contact("Sven Heinemeyer Sven.Heinemeyer@cern.ch and Thomas Hahn hahn@feynarts.de")
    convert_contact("Torbjorn Sjostrand torbjorn@thep.lu.se") # Make sure that there are no false positives
    print "-" * 75
    
    # Separator comma
    convert_contact("Brian E. Granger, Min Ragan-Kelley, zeromq-dev@lists.zeromq.org, http://zeromq.org/chatroom")
    convert_contact("david.m.cooke@gmail.com, faltet@gmail.com")
    convert_contact("Jean-loup Gailly jloup@gzip.org, Mark Adler madler@alumni.caltech.edu")
    convert_contact("Johan Alwall, Alexis Kalogeropoulos, Olivier Mattelaer")
    convert_contact("Paul Eggert, Eric Blake ebb9@byu.net")
    convert_contact("Tomer Chachamu, Wynn Wilkes, Irving Reid, Ralph Bean, Deroko")
    print "-" * 75
    
    # Separator semicolon
    convert_contact("Noah Spurrier noah@noah.org; Thomas Kluyver thomas@kluyver.me.uk; Jeff Quast contact@jeffquast.com")
    print "-" * 75
    
    # Unicode
    convert_contact("Thomas Gläßle t_glaessle@gmx.de")
    convert_contact("Honza Král honza.kral@gmail.com")
    convert_contact("Łukasz Langa lukasz@langa.pl")
    print "-" * 75
    
    # Website instead of contact
    convert_contact("http://cfif.ist.utl.pt/~paulo/index.html#contacts")
    convert_contact("http://java.com/en/download/support.jsp")
    convert_contact("http://projects.hepforge.org/gosam/")
    convert_contact("http://spark.apache.org/community.html#mailing-lists")
    convert_contact("http://xapian.org/lists")
    convert_contact("http://xerces.apache.org/mail/c-dev/")
    convert_contact("http://zeromq.org/intro:ask-for-help")
    convert_contact("http://zlib.net/mailman/listinfo/zlib-devel_madler.net")
    convert_contact("https://curl.haxx.se/mail/")
    convert_contact("https://forum.kde.org/viewforum.php?f=74")
    convert_contact("https://groups.google.com/forum/#!forum/pytoolz")
    convert_contact("https://www.mpich.org/support/")
    convert_contact("https://www.r-project.org/mail.html")
    convert_contact("https://www.riverbankcomputing.com/support/lists")
