from relinfo.models import *
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.views.decorators.cache import cache_page
import time
import logging
from django.core.paginator import Paginator
from django.http import HttpResponse
import json
from django.db.models import Q
from functional import seq
from django.db import connection
import os
from subprocess import call

logger = logging.getLogger(__name__)
CACHING_TIME = 60 * 15


@cache_page(CACHING_TIME)
def package(request, package_name=None):
    """Render single package information"""
    package = Package.objects.get(name=package_name)

    releases = Release.objects.raw('''SELECT DISTINCT r.* from relinfo_release r
                                      INNER JOIN relinfo_releasepackage rp ON rp.release_id = r.id
                                      WHERE rp.package_id = %s''', [package.id]).order_by('version')

    versions = Tag.objects.filter(package__name=package_name).order_by('name')
    return render_to_response('relinfo/package.html',
                              {
                                  'package': package,
                                  'releases': releases,
                                  'versions': versions
                              })


@cache_page(CACHING_TIME)
def packagerelease(request, package_name=None, release_id=None):
    """Render single package information"""
    package = Package.objects.filter(name=package_name)
    release = Release.objects.filter(id=release_id)
    return render_to_response('relinfo/package_release.html', {'package': package[0], 'release': release[0]})


@cache_page(CACHING_TIME)
def tag(request, package_name=None, tag_name=None):
    """Render information about a particular version of a package"""
    tag = Tag.objects.get(name=tag_name, package__name=package_name)
    rt = ReleaseTag.objects.filter(tag=tag.id)
    last_release = tag.getLastRelease()

    release = None
    if last_release:
        release = last_release.version
    release = request.GET.get('release', release)

    return render_to_response('relinfo/package_version.html', {'tag': tag, 'rt': rt, 'release': release})


@cache_page(CACHING_TIME)
def release(request, version=None):
    """Render information about a particular release"""
    releases = Release.objects.order_by('-date').all()
    release = Release.objects.get(version=version)

    # SQL query to extract all needed data at the same time
    cursor = connection.cursor()
    # If category is missing, 'Other' is used.
    cursor.execute("""SELECT COALESCE(p.category, 8), p.name, array_agg(DISTINCT t.name) as version
                      FROM relinfo_release r
                      INNER JOIN relinfo_releasetag rt ON rt.release_id = r.id
                      INNER JOIN relinfo_tag t ON t.id = rt.tag_id
                      INNER JOIN relinfo_package p ON p.id = t.package_id
                      WHERE r.version = %s
                      GROUP BY p.category, p.name
                      ORDER BY p.name""", [version])
    packagesByCategory = seq(cursor.fetchall())\
        .map(lambda x: (x[0], x[1:]))\
        .group_by_key()\
        .dict()

    categories = seq(PACKAGE_CATEGORIES)\
        .filter(lambda cat: cat[0] in packagesByCategory.keys())

    return render_to_response('relinfo/release.html',
                              {
                                  'categories': categories,
                                  'release': release,
                                  'releases': releases,
                                  'packagesByCategory': packagesByCategory,
                              })


@cache_page(CACHING_TIME)
def release_notes(request, release_version=None):
    if release_version == 'latest':
        release = Release.objects.raw('''SELECT * FROM relinfo_release
                                         WHERE version <> 'dev3' AND version <> 'dev4'
                                         ORDER BY date DESC
                                         LIMIT 1''')[0]
    else:
        release = Release.objects.get(version=release_version)

    return render_to_response('relinfo/release_notes.html', {'release': release})


@cache_page(CACHING_TIME)
def allreleases(request, version=None):
    """Render information about a all releases"""
    releases = Release.objects.order_by('version')

    externals = Package.objects.order_by('name').all()
    return render(request, 'relinfo/main.html',
                  {
                      'categories': PACKAGE_CATEGORIES,
                      'externals': externals,
                      'releases': releases,
                  })


@cache_page(CACHING_TIME)
def compare(request, version1=None, version2=None):
    """Render comparison info"""

    # A sql query performs much faster
    cursor = connection.cursor()
    cursor.execute("""SELECT COALESCE(p1.category, p2.category, 8) AS category, COALESCE(p1.name, p2.name) AS name, p1.tag, p2.tag
                      FROM (SELECT DISTINCT p.category, p.id, p.name, array_agg(DISTINCT t.name) AS tag
                            FROM relinfo_release r
                            INNER JOIN relinfo_releasetag rt ON r.id = rt.release_id
                            INNER JOIN relinfo_tag t ON rt.tag_id = t.id
                            INNER JOIN relinfo_package p ON t.package_id = p.id
                            WHERE r.version = %s
                            GROUP BY p.id, p.name) p1
                      FULL OUTER JOIN (SELECT DISTINCT p.category, p.id, p.name, array_agg(DISTINCT t.name) AS tag
                                       FROM relinfo_release r
                                       INNER JOIN relinfo_releasetag rt ON r.id = rt.release_id
                                       INNER JOIN relinfo_tag t ON rt.tag_id = t.id
                                       INNER JOIN relinfo_package p ON t.package_id = p.id
                                       WHERE r.version = %s
                                       GROUP BY p.id, p.name) p2 ON p2.id = p1.id
                      WHERE p1.tag <> p2.tag OR p1.tag IS NULL OR p2.tag IS NULL
                      ORDER BY category, name;""", [version1, version2])

    packagesByCategory = seq(cursor.fetchall())\
        .map(lambda x: (x[0], [x[1], set(x[2] or []) - set(x[3] or []), set(x[3] or []) - set(x[2] or [])]))\
        .group_by_key()\
        .dict()

    releases = Release.objects.order_by('-date').all()
    release1 = Release.objects.get(version=version1)
    release2 = Release.objects.get(version=version2)
    categories = seq(PACKAGE_CATEGORIES)\
        .filter(lambda cat: cat[0] in packagesByCategory.keys())

    return render_to_response('relinfo/compare.html',
                              {
                                  'categories': categories,
                                  'releases': releases,
                                  'release1': release1,
                                  'release2': release2,
                                  'packagesByCategory': packagesByCategory
                              })


def assign(dic, value):
    dic[value.name] = value
    return dic


@cache_page(CACHING_TIME)
def listreleases(request):
    """Renders a list of releases"""
    releases = Release.objects.all()
    return render_to_response('relinfo/listreleases.html', {'releases': releases})

def webhook(request):
    call(os.path.dirname(os.path.abspath(__file__)) + '/../webhook/regenerate_web.sh')
    return HttpResponse(status=200)

def packageList(request):
    """Ajax method used to filter the package list."""
    filter_str = request.GET.get('filter', '')
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', 'name')
    if not order_by in ['name', '-name', 'category', '-category']:
        order_by = 'name'
    versions = request.GET.get('versions', 'last')

    # Filter by name
    query = Q(name__icontains=filter_str)

    # Filter by language
    languages = seq(PACKAGE_LANGUAGES)\
        .filter(lambda t: filter_str.lower() in t[1].lower())\
        .map(lambda t: Q(language=t[0]))
    if not languages.empty():
        query |= languages.reduce(lambda x, y: x | y)

    # Filter by category
    categories = seq(PACKAGE_CATEGORIES)\
        .filter(lambda t: filter_str.lower() in t[1].lower())\
        .map(lambda t: Q(category=t[0]))
    if not categories.empty():
        query |= categories.reduce(lambda x, y: x | y)

    paginator = Paginator(Package.objects.filter(query).order_by(order_by), 50)

    response_data = {
        'num_pages': paginator.num_pages,
        'current_page': page,
        'filter_str': filter_str,
        'packages': map(lambda package: {
            'name': package.name,
            'category': package.get_category_display(),
            'language': package.get_language_display(),
            'release':  package.getLastTag,
            'contacts': map(lambda contact: {
                'name': contact.name,
                'email': contact.email
            }, package.contacts.all())
        }, paginator.page(page))
    }

    return HttpResponse(json.dumps(response_data), content_type="application/json")
