#!/usr/bin/python

import argparse
import json
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.core.exceptions import ObjectDoesNotExist
from relinfo.models import Package, PackageContact, Contact, License, PACKAGE_CATEGORIES, OTHER_CAT, PACKAGE_LANGUAGES, OTHER_LANG
from relinfo.contact_converter import convert_contact
from functional import seq


def check_package(package):
    
    package_db = Package.objects.get(name=package['name'])
    update = False
    
    attrs = ['description', 'name', 'fullname', 'homepage']
    for attr in attrs:
        package_attr = unicode(package[attr])
        if package[attr] and getattr(package_db, attr) != package_attr:
            setattr(package_db, attr, package_attr)
            update = True
    
    package_language = unicode(package['language']).strip()
    if (package['language'] and not package_db.language) or \
            (package_db.language and PACKAGE_LANGUAGES[package_db.language][1] != package_language):
        
        update = True
        
        if not package_language or package_language == '' or package_language.lower() == 'none':
            package_db.language = None
        else:
            # Take only first entry
            if ' and ' in package_language:
                # 'C and Cpp' -> 'C'
                package_language = package_language.split(' and ')[0].strip()
            elif '/' in package_language:
                # 'Fortran/Cpp' -> 'Fortran'
                 package_language = package_language.split('/')[0].strip()
            # Unify naming: 'Cpp' -> 'C++'
            if package_language == 'Cpp':
                package_language = 'C++'
            
            package_db.language = seq(PACKAGE_LANGUAGES).find(lambda l: l[1] == package_language)
            
            if not package_db.language:
                # Use 'Other' as default
                package_db.language = PACKAGE_LANGUAGES[OTHER_LANG]
            
            package_db.language = package_db.language[0]

    
    package_category = unicode(package['category']).strip()
    if (package['category'] and not package_db.category) or \
            (package_db.category and PACKAGE_CATEGORIES[package_db.category][1] != package_category):
        
        update = True
        
        if not package_category or package_category == '' or package_category.lower() == 'none':
            package_db.category = None
        else:
            # Fix typos
            if package_category == 'Database' or package_category == 'SQL':
                package_category = 'Databases'
            elif package_category == 'Tools':
                package_category = 'Tool'
            elif package_category == 'ML':
                package_category = 'Machine Learning'
            
            package_db.category = seq(PACKAGE_CATEGORIES).find(lambda l: l[1] == package_category)
            
            if not package_db.category:
                # Use 'Other' as default
                package_db.category = PACKAGE_CATEGORIES[OTHER_CAT]
            
            package_db.category = package_db.category[0]
    
    package_license = unicode(package['license']).strip()
    if (package['license'] and not package_db.license) or \
            (package_db.license and package_db.license.name != package_license):
        
        update = True
        
        if not package_license or package_license == '' or \
                package_license.lower() == 'none' or \
                package_license.lower() == 'unknown':
            package_db.license = None
        else:
            package_db.license = License.objects.get_or_create(name=package_license)[0]
    
    contacts = package_db.contacts.all()
    if 'contacts' in package:
        refreshContacts = len(package['contacts']) != package_db.contacts.count()
        
        if not refreshContacts:
            for contact in package['contacts']:
                if seq(contacts).filter(lambda c: c.name == contact['name'] and c.email == contact['email']).empty():
                    refreshContacts = True
                    update = True
                    break
        if refreshContacts:
            package_db.contacts.clear()
            
            for contact in package['contacts']:
                contact_db = Contact.objects.get_or_create(name=contact['name'], email=contact['email'])
                pc_db = PackageContact(package_id=package_db.id, contact_id=contact_db[0].id)
                pc_db.save()
    
    if update:
        print "Updating package '%s' ..." % str(package_db)
        package_db.save()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parses a json file containing information about the packages. If there is any difference with the DB, the DB is updated.')
    parser.add_argument('path', help='Path of the JSON file with the package info')
    args = parser.parse_args()
    
    if (os.path.exists(args.path)):
        print "Reading packages from '%s' ..." % args.path
        packages = json.load(open(args.path, 'r'))
        
        for package in packages:
            check_package(package)
    else:
        print "The file '%s' does not exist." % args.path
