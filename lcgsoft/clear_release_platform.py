#!/usr/bin/python

import sys
import os
os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
from relinfo.models import Release, ReleaseTag
from django.db.models import Q
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Removes the selected platforms from a given release.')
    parser.add_argument('release', help='The name of the release')
    parser.add_argument('platforms', help='The platforms to be deleted', nargs='+')
    args = parser.parse_args()
    
    try:
        release = Release.objects.get(version=args.release)
        for platform in args.platforms:
            print "Clearing release '%s' for platform '%s' ..." % (release, platform)
            release.releaseplatform_set.filter(platform__name=platform).delete()
            release.releasetag_set.filter(platform__name=platform).delete()
    except:
        print "Release '%s' does not exist in th database and thus does not need to be cleaned." % release
