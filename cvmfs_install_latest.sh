#!/bin/bash -x
sudo -i -u cvsft<<EOF
shopt -s nocasematch
for iterations in {1..10}
do
  cvmfs_server transaction sft-nightlies.cern.ch
  if [ "\$?" == "1" ]; then
    if  [[ "\$iterations" == "10" ]]; then
      echo "Too many tries... "
      exit 1
    else
      echo "Transaction is already open. Going to sleep..."
      sleep 10m
    fi
  else
    break
  fi
done

$WORKSPACE/lcgcmake/cmake/scripts/install_binary_repository.py --url http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/latest \
                                                               --platform $PLATFORM \
                                                               --prefix /cvmfs/sft-nightlies.cern.ch/lcg/latest \
                                                               --lcgprefix /cvmfs/sft-nightlies.cern.ch/lcg/latest:/cvmfs/sft.cern.ch/lcg/releases \
                                                               --maxpack $MAXPACK
if [ "\$?" != "0" ]; then
  echo "There is an error installing the packages. Exiting ..."
  cd $HOME
  cvmfs_server abort -f sft-nightlies.cern.ch
  exit 1
fi

cd $HOME
cvmfs_server publish sft-nightlies.cern.ch

EOF
