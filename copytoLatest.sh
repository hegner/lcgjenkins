#!/bin/bash -x
# This script is intended to copy binary tarfiles to the 'latest' in EOS

export EOS_MGM_URL=root://eosuser.cern.ch

if [[ $PLATFORM == *mac* ]]; then
    kinit -V -k -t /build/conf/sftnight.keytab
else
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/latest
node=lcgapp-centos7-physical2.cern.ch
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt

if [ -d $WORKSPACE/docker/build ]; then
  cd $WORKSPACE/docker/build
else
  cd $WORKSPACE/build
fi

if [[ ${PLATFORM} == *-slc6* || ${PLATFORM} == *-cc7* || ${PLATFORM} == *-centos* || ${PLATFORM} == *-ubuntu* ]]; then
  xrdcp -f $txtfile root://eosuser.cern.ch/$basespace/$txtfile
  if [ "$(ls -A tarfiles)" ]; then
    echo "Take action: tarfiles is not Empty"
    cd tarfiles
    for file in *.tgz; do
      xrdcp -f ${file} root://eosuser.cern.ch/${basespace}/${file}
    done
  else
    echo "tarfiles is Empty. I will assume this is not an error though"
    exit 0
  fi
else
  scp -B $PWD/$txtfile sftnight@$node:$basespace
  if [ "$(ls -A tarfiles)" ]; then
    cd tarfiles
    files=`ls -1 -d *.tgz`
    for file in $files;  do
        scp -B $PWD/$file sftnight@$node:$basespace
    done
  else
    echo "tarfiles directory is empty. I will assume this is not an error though"
    exit 0
  fi
fi
ssh sftnight@$node "$basespace/make-summaries $PLATFORM"
exit 0
