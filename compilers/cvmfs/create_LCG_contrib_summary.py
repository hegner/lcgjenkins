#!/usr/bin/env python
# Add lcg python common module to PYTHONPATH
import sys, os
sys.path.append("{}/../../python".format(os.path.dirname(os.path.realpath(__file__))))

from lcg.buildinfo import read_buildinfo_file

def simple_platform(platform):
    # Take only arch and os, ignore compiler and mode
    items = platform.split("-")
    return '-'.join(items[:2])

def create_contrib_summary(filename, destdir="./"):
    buildinfo = read_buildinfo_file(filename)
    if not buildinfo:
        raise Exception("File cannot be interpreted. Does not contain buildinfo. File: {}".format(filename))
    platform = simple_platform(buildinfo[0]["PLATFORM"])
    
    with open("{}/LCG_contrib_{}.txt".format(destdir, platform), "w") as f:
        for item in buildinfo:
            f.write("{NAME}; {HASH}; {VERSION}; ./{NAME}/{VERSION}/{PLATFORM}; {DEPS}\n"
                        .format(DEPS=','.join(["{}-{}".format(d[0],d[2]) for d in item["DEPENDENCIES"]]), **item))
    


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Create LCG_contrib_<platform>.txt file")
    parser.add_argument("--outputdir", default="./")
    parser.add_argument("filename", type=str, nargs="+", help="Concatenated buildinfo files created after LCGCMake finishes compilation")

    args = parser.parse_args()

    for filename in args.filename:
        create_contrib_summary(filename, destdir=args.outputdir)
        
