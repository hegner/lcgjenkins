#!/bin/bash
shopt -s nocasematch

RELEASE_URL="http://lcgpackages.web.cern.ch/lcgpackages/tarFiles/releases"
LCG_CVMFS=/cvmfs/sft.cern.ch/lcg

for iterations in {1..10}
do
  cvmfs_server transaction $REPOSITORY
  if [ "$?" == "1" ]; then
    if  [[ "$iterations" == "10" ]]; then
      echo "Too many tries... "
    else
       echo "Transaction is already open. Going to sleep..."
       sleep 10m
    fi
  else
    break
  fi
done

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
array=(${PLATFORM//-/ })
comp=`echo ${array[2]}`

abort=0

echo "This is the value of the BUILDMODE: ---> ${BUILDMODE}"

$WORKSPACE/lcgjenkins/lcginstall.py -u $RELEASE_URL --nolinks -r contrib -d LCG_${LCG_VERSION}_$PLATFORM.txt -p $LCG_CVMFS/releases -e cvmfs
if [ "$?" == "0" ]; then
  echo "Installation script has worked, we go on"
  abort=1
else
  echo "there is an error installing the packages. Exiting ..."
  cd $HOME
  cvmfs_server abort -f sft.cern.ch
  exit 1
fi

cd  /cvmfs/sft.cern.ch/lcg/releases
wget -O LCG_contrib.txt $RELEASE_URL/LCG_${LCG_VERSION}_${PLATFORM}.txt
$WORKSPACE/lcgjenkins/compilers/cvmfs/create_LCG_contrib_summary.py LCG_contrib.txt 

# Create setup.{sh,csh}
echo "Create setup.{sh,csh}"
for package in `python $WORKSPACE/lcgjenkins/python/lcg/buildinfo.py --pattern "{NAME}/{VERSION}-{HASH}/{PLATFORM}" LCG_contrib.txt`; do
    $WORKSPACE/lcgjenkins/compilers/source_env/create_setup.py --platform $PLATFORM $package
done

# Create link structure in contrib
if [[ "$LINK_CONTRIB" == "true" ]]; then
    echo "Create links in contrib"
    for package in `$WORKSPACE/lcgjenkins/python/lcg/buildinfo.py --pattern "{NAME}/{VERSION}-{HASH}/{PLATFORM}" LCG_contrib.txt`; do
        PKG_NAME=${package/\/*/} 
        $WORKSPACE/lcgjenkins/compilers/cvmfs/link_contrib.sh --verbose --short-platform \
                        --platform $PLATFORM --name $PKG_NAME $package $LCG_CVMFS/contrib
    done
fi
rm LCG_contrib.txt

echo "Finished installation, starting publishing into cvmfs"

cd $HOME
cvmfs_server publish sft.cern.ch
