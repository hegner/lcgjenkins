#!/bin/bash -x
touch controlfile
source $WORKSPACE/lcgjenkins/jk-setup.sh Release $COMPILER $LCG_VERSION

if [[ $LABEL = *slc6* ]]; then
    # In order to compile llvm, it is required a python version >=2.7
    # Because slc6 contains python2.6, it is not possible to compile llvm and therefor we need to source newer python.
    # Python for llvm is only a build dependency, it is not required at runtime
    # During the compilation it is also required to have jinja2 to create setup.{sh,csh} files. 
    source /cvmfs/sft.cern.ch/lcg/releases/LCG_93/Jinja2/2.10/x86_64-slc6-gcc62-opt/Jinja2-env.sh
    echo Using /cvmfs/sft.cern.ch/lcg/releases/LCG_93/Jinja2/2.10/x86_64-slc6-gcc62-opt/Jinja2-env.sh from cvmfs
elif [[ $LABEL = *centos7* ]]; then
    # During the compilation it is also required to have jinja2 to create setup.{sh,csh} files. 
    source /cvmfs/sft.cern.ch/lcg/releases/LCG_93/Jinja2/2.10/x86_64-centos7-gcc62-opt/Jinja2-env.sh
    echo Using /cvmfs/sft.cern.ch/lcg/releases/LCG_93/Jinja2/2.10/x86_64-centos7-gcc62-opt/Jinja2-env.sh from cvmfs
else
    echo Python dependency satisfied 
    python --version
fi

# Print env
env

# Setting THIS to lcgjenkins for compatibility with other scrips
THIS=$(readlink -f $WORKSPACE/lcgjenkins)
PLATFORM=`$WORKSPACE/lcgjenkins/getPlatform.py`

ctest -VV -S $WORKSPACE/lcgjenkins/compilers/lcgcmake-build-compiler.cmake

$WORKSPACE/lcgjenkins/copytoEOS.sh $LCG_VERSION

cat > $WORKSPACE/jenkins.properties <<EOF
VERSION_MAIN=${LCGCMAKE_REPO_BRANCH}
VERSION_JENKINS=${LCGJENKINS_REPO_BRANCH}
PLATFORM=${PLATFORM}
BUILDMODE=opt
LCG_VERSION=${LCG_VERSION}
EOF

