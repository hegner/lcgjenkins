cmake_minimum_required(VERSION 2.8)

#---Common Geant4 CTest script----------------------------------------------
set(CTEST_SCRIPT_DIRECTORY ${CTEST_SCRIPT_DIRECTORY}/..)
include(${CTEST_SCRIPT_DIRECTORY}/common.cmake)

find_program(CTEST_GIT_COMMAND NAMES git)
set(CTEST_UPDATE_COMMAND ${CTEST_GIT_COMMAND})
if(NOT "$ENV{GIT_COMMIT}" STREQUAL "")  #--- From Jenkins---------------------
  # set(CTEST_CHECKOUT_COMMAND "cmake -E chdir ${CTEST_SOURCE_DIRECTORY} ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_PREVIOUS_COMMIT}")
  set(CTEST_GIT_UPDATE_CUSTOM  ${CTEST_GIT_COMMAND} checkout -f $ENV{GIT_COMMIT})
endif()


set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_COMMAND "make -k -j${ncpu} $ENV{TARGET}")

execute_process(COMMAND python ${CTEST_SCRIPT_DIRECTORY}/getPlatform.py OUTPUT_VARIABLE platform)
string(REGEX REPLACE "\n$" "" platform ${platform})
set(target $ENV{TARGET})
set(CTEST_BUILD_NAME "${platform}-${target}")

#---Notes to be attached to the build---------------------------------------
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_SOURCE_DIRECTORY}/cmake/toolchain/heptools-$ENV{LCG_VERSION}.cmake)
set(CTEST_NOTES_FILES ${CTEST_NOTES_FILES} ${CTEST_BINARY_DIRECTORY}/dependencies.json)

#---CTest commands----------------------------------------------------------
#ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
file(REMOVE_RECURSE ${CTEST_BINARY_DIRECTORY})

if("$ENV{CLEAN_INSTALLDIR}" STREQUAL "true")
  file(REMOVE_RECURSE ${CTEST_INSTALL_DIRECTORY})
endif()

set(ignore $ENV{LCG_IGNORE})
set(options -DLCG_VERSION=$ENV{LCG_VERSION}
            -DCMAKE_INSTALL_PREFIX=${CTEST_INSTALL_DIRECTORY}
            -DLCG_INSTALL_PREFIX=$ENV{LCG_INSTALL_PREFIX}
            -DLCG_SAFE_INSTALL=ON
            -DLCG_IGNORE=${ignore}
            -DCMAKE_VERBOSE_MAKEFILE=OFF
            $ENV{LCG_EXTRA_OPTIONS})

set(lcg_ignore ${})
# The build mode drives the name of the slot in cdash
ctest_start(Release TRACK Compilers)
ctest_update()
ctest_configure(BUILD   ${CTEST_BINARY_DIRECTORY} 
                SOURCE  ${CTEST_SOURCE_DIRECTORY}
                OPTIONS "${options}")
ctest_build(BUILD ${CTEST_BINARY_DIRECTORY})
file(STRINGS ${CTEST_BINARY_DIRECTORY}/fail-logs.txt logs)
ctest_upload(FILES ${logs})
ctest_submit()




