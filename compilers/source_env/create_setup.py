#!/usr/bin/env python
import sys, os
sys.path.append("{}/../../python".format(os.path.dirname(os.path.realpath(__file__))))

from lcg.buildinfo import read_buildinfo_file

template_dir = os.path.dirname(os.path.realpath(__file__)) + "/envs"

def compile_setup_content(name, data, shell="sh"):
    from jinja2 import Environment, FileSystemLoader, select_autoescape
    env = Environment(
        loader=FileSystemLoader(template_dir) # './envs' directory
    )

    template = env.select_template(['{}.{}.in'.format(name, shell), 'common.{}.in'.format(shell)])
    return template.render(**data)

def create_setup_content(path, shell="sh", platform=None):
    data = read_buildinfo_file(path + "/*")[0]
    if platform:
        data["PLATFORM"] = platform
    name = data["NAME"]
    return compile_setup_content(name, data, shell)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='LCG setup file creator', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    #parser.add_argument("--check", help="Check integrity of the setup file (source the file)") 
    parser.add_argument("--platform", type=str, help="Specify explicit platform") 
    parser.add_argument("path", type=str, help="Path to directory containing buildinfo file, as well as the target directory for setup file")

    args = parser.parse_args()
    path = args.path

    for shell in ["sh", "csh"]:
        setupfile = os.path.join(path, "setup.{}".format(shell))
        setup_content = create_setup_content(path, shell, platform=args.platform)
        with open(setupfile, "w") as f:
            f.write(setup_content)

