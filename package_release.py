#!/usr/bin/python

import os, sys
import Tools
import common_parameters
from optparse import OptionParser
from copyRPMS import copyRPMS
from getNewRPMrevision import get_new_RPM_revision


def extractLCGSummary(scriptdir,platform,lcg_version, policy):
    command = "%s/lcgjenkins/extract_LCG_summary.py . %s %s %s" %(scriptdir, platform, lcg_version, policy)
    Tools.executeAndCheckCommand(command,"Extracting LCG summary","ERROR extracting LCG summary")

def createSpecFiles(workdir,platform,rpm_revision):
    command = "%s/lcgjenkins/LCGRPM/package/createLCGRPMSpec.py LCG_externals_%s.txt -b %s/packaging -o externals.spec --release %s" %(scriptdir, platform, workdir, rpm_revision)
    Tools.executeAndCheckCommand(command,"creating specfile for externals","ERROR creating specfile for externals")
    command = "%s/lcgjenkins/LCGRPM/package/createLCGRPMSpec.py LCG_generators_%s.txt -b %s/packaging -o generators.spec --release %s" %(scriptdir, platform, workdir, rpm_revision)
    Tools.executeAndCheckCommand(command,"creating specfile for generators","ERROR creating specfile for generators")

def buildRPMs():
    command = "rpmbuild -bb externals.spec"
    Tools.executeAndCheckCommand(command,"building externals RPMs", "ERROR building externals RPMs")
    command = "rpmbuild -bb generators.spec"
    Tools.executeAndCheckCommand(command,"building generators RPMs", "ERROR generators RPMs")

##########################
if __name__ == "__main__":
  # extract command line parameters
  usage = "usage: %prog scriptdir workdir lcg_version platform target policy"
  parser = OptionParser(usage)
  (options, args) = parser.parse_args()
  if len(args) != 7:
    parser.error("incorrect number of arguments.")
  else:
    scriptdir     =  args[0]
    workdir       =  args[1]
    lcg_version   =  args[2]
    platform      =  args[3]
    target        =  args[4]
    policy        =  args[5]
    compiler      =  args[6]

  if policy not in ("release", "Generators", "Rebuild"):
    print "ERROR: unknown install/copy policy"
    sys.exit(1)

  os.environ['EOS_MGM_URL'] = 'root://eosproject-l.cern.ch'

  command = "kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab"
  Tools.executeAndCheckCommand(command,"Token creation to write in EOS", "ERROR creating a Kerberos token")

  tmparea = common_parameters.tmparea
  # do the job
  extractLCGSummary(scriptdir,platform,lcg_version, policy)
  if policy in ("Generators", "Rebuild"):
    rpm_revision = get_new_RPM_revision(lcg_version, platform)
  else:
    rev_number = os.environ['RPM_REVISION_NUMBER'] if 'RPM_REVISION_NUMBER' in os.environ else 'a'
    if rev_number.isdigit():
      rpm_revision = rev_number
    else:
      rpm_revision = Tools.extractLCGNumber(lcg_version)
  createSpecFiles(workdir,platform,rpm_revision)
  buildRPMs()
  rpmsarea = "%s/LCG_%s" %(tmparea, lcg_version)
  copyRPMS(workdir, rpmsarea, target=target, policy=policy)
