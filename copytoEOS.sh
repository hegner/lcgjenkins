#!/bin/bash -x

set -e  # Fail on first error

export EOS_MGM_URL="root://eosproject-l.cern.ch"
# export PLATFORM=$(grep $WORKSPACE/properties.txt -e PLATFORM | cut -d'=' -f2)
FILES=*.tgz
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt
isDone=isDone-${PLATFORM}
isDoneUnstable=isDone-unstable-${PLATFORM}

# Retries a command a with an exponential backoff strategy.
# Based on https://gist.github.com/fernandoacorreia/b4fa9ae88c67fa6759d271b743e96063
function retry {
    local current_attempt=1
    local max_attempts=5
    local timeout_seconds=1

    set -o pipefail

    while (( ${current_attempt} <= ${max_attempts} ))
    do
        set +e
        "$@"

        if [[ $? == 0 ]]
        then
            # Command successful
            break
        elif (( ${current_attempt} >= ${max_attempts} ))
        then
            echo
            echo "[ERROR] Attempt ${current_attempt}/${max_attempts} failed ($@). Maximum retries exceeded. Attempting to continue." 1>&2
        else
            echo
            echo "[WARNING] Attempt ${current_attempt}/${max_attempts} failed ($@). Retrying in ${timeout_seconds}s..." 1>&2
            sleep ${timeout_seconds}
        fi
        current_attempt=$(( current_attempt + 1 ))
        timeout_seconds=$(( timeout_seconds * 2 ))
    done
    set -e
}

if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/${LCG_VERSION}/${weekday}
else
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/releases
    metaspace=/eos/project/l/lcg/www/lcgpackages/lcg/meta
fi

# Check that EOS is mounted and available
ls /eos/project/l/lcg/www/lcgpackages/tarFiles 1>/dev/null || (echo "EOS not available! Aborting."; exit 1)

if [ -d $WORKSPACE/docker/build ]; then
  cd $WORKSPACE/docker/build
else
  cd $WORKSPACE/build
fi

if [[ ${PLATFORM} == *-slc6* || ${PLATFORM} == *-cc7* || ${PLATFORM} == *-centos7* || ${PLATFORM} == *-ubuntu* || ${PLATFORM} == *-cc8* || ${PLATFORM} == *-centos8* ]]; then
    if [ "${BUILDMODE}" == "nightly" ]; then

        # Create nightlies directory if it's missing
        [ -d ${basespace} ] || mkdir -p ${basespace}

        # Check that file exists, as missing files on EOS return 'permission denied'
        if [ -f $basespace/$isDone ]; then
            rm -vf $basespace/$isDone
        fi
        if [ -f $basespace/$isDoneUnstable ]; then
            rm -vf $basespace/$isDoneUnstable
        fi

        # Copy updated isDone files
        if [ -f $isDone ]; then
            retry xrdcp $isDone ${EOS_MGM_URL}/$basespace/$isDone
        fi
        if [ -f $isDoneUnstable ]; then
            retry xrdcp $isDoneUnstable ${EOS_MGM_URL}/$basespace/$isDoneUnstable
        fi
    fi

    retry xrdcp -f $txtfile ${EOS_MGM_URL}/$basespace/$txtfile
    if [ "${BUILDMODE}" != "nightly" ]; then
       retry xrdcp -f $txtfile ${EOS_MGM_URL}/$metaspace/$txtfile
    fi

    if [ "$(ls -A tarfiles)" ]; then
        echo "Take action: tarfiles is not Empty"
        cd tarfiles
        for files in $FILES
        do
            if [ "${BUILDMODE}" == "nightly" -o "${BUILDMODE}" == "Rebuild" ]; then
                retry xrdcp -f ${files} ${EOS_MGM_URL}/${basespace}/${files}
            else
                # Don't copy if the file exits, is regular and has size > 0
                # This allows for copying in the event of EOS corruption
                if [ -f ${basespace}/${files} -a -s ${basespace}/${files} ]; then
                    echo "The file already exists in EOS"
                else
                    retry xrdcp ${files} ${EOS_MGM_URL}/${basespace}/${files}
                fi
            fi
        done
    else
        echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
else
    if [ "${BUILDMODE}" == "nightly" ]; then
        ssh sftnight@lcgapp-centos7-physical2.cern.ch "rm -vf ${basespace}/${isDone}"
        ssh sftnight@lcgapp-centos7-physical2.cern.ch "rm -vf ${basespace}/${isDoneUnstable}"
        retry scp ${isDone} ${isDoneUnstable} sftnight@lcgapp-centos7-physical2.cern.ch:${basespace}
    fi

    ssh -o StrictHostKeyChecking=no sftnight@lcgapp-centos7-physical2.cern.ch "rm -vf ${basespace}/${txtfile}"
    retry scp -o StrictHostKeyChecking=no ${txtfile} sftnight@lcgapp-centos7-physical2.cern.ch:${basespace}

    if [ "$(ls -A tarfiles)" ]; then
        echo "Take action: tarfiles is not Empty"
        cd tarfiles
        retry scp -o StrictHostKeyChecking=no *tgz sftnight@lcgapp-centos7-physical2.cern.ch:${basespace}
    else
        echo "tarfiles is Empty. I will assume this is not an error though"
        exit 0
    fi
fi
