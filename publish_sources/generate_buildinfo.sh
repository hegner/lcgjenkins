#!/usr/bin/env bash

# Generate a buildinfo summary for a given release and upload it to EOS.
# These files are used by the source publishing job.

set -e           # Fail script on first error

TODAY="$(date +%a)"

# Check environment variables, exit if they are not set
# Expecting values like REPOSITORY=sft.cern.ch, TODAY=Mon, LCG_VERSION=LCG_88, PLATFORM=x86_64-centos7-gcc62-opt
echo
echo "------------------------------------------------------------------------------------------"
echo "REPOSITORY:  ${REPOSITORY:?You need to set REPOSITORY (non-empty)}"
echo "TODAY:       ${TODAY:?You need to set TODAY (non-empty)}"
echo "LCG_VERSION: ${LCG_VERSION:?You need to set LCG_VERSION (non-empty)}"
echo "PLATFORM:    ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "You can set a URL filter such as 'MCGenerator' by defining 'PKG_FILTER'"
echo "------------------------------------------------------------------------------------------"
echo

HERE=$(pwd)
SOURCE_LIST="${HERE}/source_list_${LCG_VERSION}.txt"

BASESPACE="/eos/project/l/lcg/www/lcgpackages/source_buildinfo"
export EOS_MGM_URL="root://eosproject-l.cern.ch"

# Retries a command a with an exponential backoff strategy.
# Based on https://gist.github.com/fernandoacorreia/b4fa9ae88c67fa6759d271b743e96063
function retry {
    local current_attempt=1
    local max_attempts=5
    local timeout_seconds=1

    set -o pipefail

    while (( ${current_attempt} <= ${max_attempts} ))
    do
        set +e
        "$@"

        if [[ $? == 0 ]]
        then
            # Command successful
            break
        elif (( ${current_attempt} >= ${max_attempts} ))
        then
            echo
            echo "[ERROR] Attempt ${current_attempt}/${max_attempts} failed ($@). Maximum retries exceeded. Aborting" 1>&2
            exit 1
        else
            echo
            echo "[WARNING] Attempt ${current_attempt}/${max_attempts} failed ($@). Retrying in ${timeout_seconds}s..." 1>&2
            sleep ${timeout_seconds}
        fi
        current_attempt=$(( current_attempt + 1 ))
        timeout_seconds=$(( timeout_seconds * 2 ))
    done
    set -e
}

# Clone directory if it does not already exist
LCGCMAKE_DIR="${HERE}/lcgcmake"
echo "[INFO] Cloning lcgcmake, branch: ${LCG_VERSION}"
if [ ! -d "${LCGCMAKE_DIR}" ]; then
    git clone --single-branch --branch ${LCG_VERSION} "https://gitlab.cern.ch/sft/lcgcmake.git" ${LCGCMAKE_DIR}
fi

# Set up cmake and a compiler
# TODO: Assumes CMake version, compiler version and platform and 
echo "[INFO] Configuring CMake"
export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.8.1/Linux-x86_64/bin:${PATH}
. /cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0binutils/${PLATFORM}/setup.sh
export FC=`which gfortran`
export CXX=`which g++`
export CC=`which gcc`

# Generate the buildinfo summary for this version to obtain the source list
BUILD_DIR="${HERE}/build"
echo "[INFO] Extracting source list to ${BUILD_DIR}"
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}
LCG_VER_SHORT=$(echo ${LCG_VERSION} | sed -e 's/LCG_//')
cmake -DCMAKE_INSTALL_PREFIX=/dev/null -DLCG_VERSION=${LCG_VER_SHORT} ${LCGCMAKE_DIR}
sed '1d' ${LCG_VERSION}_${PLATFORM}.txt > ${SOURCE_LIST}
cd ${HERE}

# Upload the file to EOS
xrdfs ${EOS_MGM_URL} mkdir -p "${BASESPACE}"
retry xrdcp -f ${SOURCE_LIST} "${EOS_MGM_URL}/${BASESPACE}/"

# Cleanup
cd ${HERE}
echo "Cleaning up"
rm -rf ${LCGCMAKE_DIR}
rm -rf ${BUILD_DIR}
rm -fv ${SOURCE_LIST}

exit 0
