#!/usr/bin/env bash
set +x # Disable debug prints of every command
set -e # Fail this script if an error occurs

# Show the job configuration and set defaults. The values can be overwritten externally if necessary
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo "  REPOSITORY_NAME: ${REPOSITORY_NAME:=hadoop-confext}"
echo "  TARGET_CVMFS:    ${TARGET_CVMFS:=/cvmfs/sft.cern.ch/lcg/etc}"
# Run 'compare' on any build node, but 'publish' only on a CVMFS release manager
echo "  HOSTNAME:        $( hostname )"
echo "  WORKSPACE:       ${WORKSPACE:?You need to set WORKSPACE}"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

case "$1" in
    compare )
        if diff --recursive --minimal --exclude=".cvmfscatalog" --exclude=".git" \
            ${WORKSPACE}/${REPOSITORY_NAME} ${TARGET_CVMFS}/${REPOSITORY_NAME}
        then
            echo "[INFO] Folders are the same -> Nothing to do"
        else
            echo "[INFO] There's a difference -> Publish changes to CVMFS"

            # Trigger publication Jenkins job with the existence of the file "PUBLISH_TO_CVMFS"
            touch ${WORKSPACE}/PUBLISH_TO_CVMFS
        fi
        ;;
    publish )
        # Use the keytab file on cvmfs-sft.cern.ch that is in a different place than usual
        kinit sftnight@CERN.CH -5 -V -k -t /var/spool/cvmfs/sft.cern.ch/sftnight/build/conf/sftnight.keytab
        echo

        # Clone git manually because the git plugin in this version of Jenkins is buggy on SLC 6
        git clone --depth 1 https://:@gitlab.cern.ch:8443/awg/${REPOSITORY_NAME}.git
        echo

        sudo -i -u cvsft <<EOF
cvmfs_server transaction sft.cern.ch
if [[ "\$?" != "0" ]]
then
    echo
    echo "[WARN] Transaction is already open. Exiting ..."
    exit 1
else
    set -x
    rsync --archive \
          --verbose \
          --human-readable \
          --checksum \
          --delete \
          --exclude=".git/*" \
          ${WORKSPACE}/${REPOSITORY_NAME} ${TARGET_CVMFS}
    set +x
    echo
    echo "[INFO] Publishing changes ..."
    echo
    cvmfs_server publish sft.cern.ch 
fi
EOF
        ;;
    * )
        echo "ERROR: The command '$1' doesn't exist. Please use either 'compare' or 'publish'"
        exit 1
        ;;
esac

echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo

