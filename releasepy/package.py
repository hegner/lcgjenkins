# -*- coding: utf-8 -*-

"""
releasepy.package
~~~~~~~~~~~~~~~
This module contains the definition of a Package.
"""

import os

class Package(object):
    """Description of the package
    :param name: name of the package
    :param version: version of the package
    :param directory: path where the package is located
    :param platform: operative system where is installed
    :param compiler: compiler used during the installation
    """

    def __init__(self, name, version, directory, platform, compiler):
        self.name = name
        self.version = version
        self.directory = directory
        self.platform = platform
        self.compiler = compiler

    def getPackageName(self):
        return self.name

    def getDirectory(self):
        return self.directory

    def getVersion(self):
        return self.version

    def getPlatform(self):
        return self.platform

    def getCompiler(self):
        return self.compiler

    def getName(self):
        return "{0}-{1}".format(self.name, self.version)

    def getInstallPath(self):
        return os.path.join(self.directory, self.version, self.platform)


class ExtendedPackage(Package):
    """Description of the package
    :param hashstr: calculated hash of the package
    :param dependencies: dependencies of the package
    """

    def __init__(self, name, version, hashstr, directory, dependencies, platform, compiler):
        Package.__init__(self, name, version, directory, platform, compiler)
        self.hashstr = hashstr
        self.dependencies = dependencies

    def getHash(self):
        return self.hashstr

    def getPackageFilename(self):
        return "{0}-{1}_{2}-{3}.tgz".format(self.name, self.version, self.hashstr, self.platform)

    def getModifiedInstallPath(self):
        return os.path.join(self.directory, "{0}-{1}".format(self.version, self.hashstr), self.platform)
