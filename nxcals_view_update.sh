#!/usr/bin/env bash
set -e # 'e' quits the script on error, 'x' prints every command for debugging 

#####################################################################################
#  This script extracts the two NXCALS packages to CVMFS if there are new versions  #
#  on EOS. It creates a copy of the Python 3 view of the latest stable LCG release  #
#  and modifies it so that the new version are taken into account (see JIRA issue   #
#  SPI-1155). The script must be run as user cvsft on a CVMFS release manager in    #
#  order to be able to write to CVMFS.                                              #
#                                                                                   #
#  Created 2018-12-07 by Johannes Heinz (technical student) on request by SWAN      #
#  Requirements: bash, find, sort, tail, sed, grep, tar, unzip, xrdcp, kinit        #
#####################################################################################

if ${DEBUG_SCRIPT}; then
    echo "Enabling debug prints ..."
    set -x
fi

# Check environment variables, exit if they are not set
# Expecting values like LCG_RELEASE=94python3, PLATFORM=x86_64-centos7-gcc7-opt
echo
echo "------------------------------------------------------------------------------------------"
echo "RELEASE:  LCG_${LCG_RELEASE:?You need to set LCG_RELEASE (non-empty)}"
echo "PLATFORM: ${PLATFORM:?You need to set PLATFORM (non-empty)}"
echo "------------------------------------------------------------------------------------------"
echo

if [[ "${LCG_RELEASE}" != *"python3"* ]]; then
    echo "Error: The release '${LCG_RELEASE}' must be a Python 3 release"
    exit 1
fi

# Define the names of paths, directories and files
CVMFS_PATH="/cvmfs/sft.cern.ch/lcg/views"

SOURCE_VIEW="${CVMFS_PATH}/LCG_${LCG_RELEASE}/${PLATFORM}"
TARGET_VIEW="${CVMFS_PATH}/LCG_${LCG_RELEASE}_nxcals/${PLATFORM}"

OLD_VERSION_FILE="${TARGET_VIEW}/nxcals/nxcals_versions.txt"
PYTHON_VER=`ls -l ${SOURCE_VIEW}/bin | awk '{ print $9 }' | grep "^python[0-9]\.[0-9]\+$" | head -1` 

# Check if a nxcals view for the specified release and platform already exists
if [[ -d "${TARGET_VIEW}" && ! -L "${TARGET_VIEW}" ]]; then
    
    # Read old versions from file
    source ${OLD_VERSION_FILE}
    
    # Remove the previous version of the nxcals packages if there is an update
    if [[ ${NXCALS_JAVA_VERSION} != ${NXCALS_JAVA_VERSION_NEW} ]] || [[ ${NXCALS_PYTHON_VERSION} != ${NXCALS_PYTHON_VERSION_NEW} ]]; then
        echo "There is an update available."
    else
        echo "No updates available. Exiting ..."
        exit 5
    fi
else
    if [[ -d "${SOURCE_VIEW}" && ! -L "${SOURCE_VIEW}" ]]; then
        
        # Set initial value for the old versions to force an update
        echo "There is no previous version of NXCALS. Setting old versions to '0.0.0' ..."
        NXCALS_JAVA_VERSION=0.0.0
        NXCALS_PYTHON_VERSION=0.0.0
        
        # Create target view by copying the view of the latest stable python 3 LCG release
        mkdir ${CVMFS_PATH}/LCG_${LCG_RELEASE}_nxcals
        cp -r ${SOURCE_VIEW} ${TARGET_VIEW}
    else
        echo "Error: The view for release '${LCG_RELEASE}' and platform '${PLATFORM}' doesn't exist!"
        echo "       It is needed as a base for the new NXCALS view since we don't build it from scratch."
        exit 1
    fi
fi

# Prepare the directory for the new packages
mkdir -p ${TARGET_VIEW}/nxcals


# Check if Java NXCALS package got an update
# ------------------------------------------
if [[ ${NXCALS_JAVA_VERSION} != ${NXCALS_JAVA_VERSION_NEW} ]]; then 
    
    # Clean previous installation if necessary
    rm -rf ${TARGET_VIEW}/nxcals/${NXCALS_JAVA}
    rm -rf ${TARGET_VIEW}/lib/nxcals
    
    cd ${TARGET_VIEW}/nxcals
    
    # Copy files from Jenkins workspace to local folder before extraction
    cp ${WORKSPACE}/${NXCALS_JAVA}.tar.gz ${TARGET_VIEW}/nxcals
    
    # Unpack package and remove tarball
    mkdir ${TARGET_VIEW}/nxcals/${NXCALS_JAVA}
    tar -xvzf ${NXCALS_JAVA}.tar.gz --directory ${NXCALS_JAVA}
    rm -rf ${NXCALS_JAVA}.tar.gz
    
    # Create symbolic links
    ln -s ${TARGET_VIEW}/nxcals/${NXCALS_JAVA} ${TARGET_VIEW}/lib
    cd ${TARGET_VIEW}/lib
    mv ${NXCALS_JAVA} nxcals
    ls -hl ${TARGET_VIEW}/lib | grep nxcals
    
    cd ${TARGET_VIEW}
    GENERATION_PATTERN="#   Generated: "
    MODIFICATION_PATTERN="#   Modified for NXCALS: "
    MODIFICATION_MESSAGE="${MODIFICATION_PATTERN}$(date --universal)"
    
    # Modify setup.sh: Add modification time stamp and set SPARK_DIST_CLASSPATH if necessary
    if grep -q "${MODIFICATION_PATTERN}" setup.sh
    then
        sed -i "s/${MODIFICATION_PATTERN}.*/${MODIFICATION_MESSAGE}/" setup.sh
    else
        sed -i "/${GENERATION_PATTERN}.*/a ${MODIFICATION_MESSAGE}" setup.sh
        echo "" >> setup.sh
        echo "#---then NXCALS" >> setup.sh
        echo "export SPARK_DIST_CLASSPATH=\"\${thisdir}/nxcals/${NXCALS_JAVA}\"" >> setup.sh
    fi
    
    # Modify setup.csh: Add modification time stamp and set SPARK_DIST_CLASSPATH if necessary
    if grep -q "${MODIFICATION_PATTERN}" setup.csh
    then
        sed -i "s/${MODIFICATION_PATTERN}.*/${MODIFICATION_MESSAGE}/" setup.csh
    else
        sed -i "/${GENERATION_PATTERN}.*/a ${MODIFICATION_MESSAGE}" setup.csh
        echo "" >> setup.csh
        echo "#---then NXCALS" >> setup.csh
        echo "setenv SPARK_DIST_CLASSPATH \${thisdir}/nxcals/${NXCALS_JAVA}" >> setup.csh
    fi
    
    # Update version in the version file
    echo "NXCALS_JAVA_VERSION=${NXCALS_JAVA_VERSION_NEW}" > ${OLD_VERSION_FILE}
    echo "NXCALS_PYTHON_VERSION=${NXCALS_PYTHON_VERSION}" >> ${OLD_VERSION_FILE}
    
    echo
    echo "------------------------------------------------------------------------------------------"
    echo "Updated NXCALS Java package from version ${NXCALS_JAVA_VERSION} to ${NXCALS_JAVA_VERSION_NEW}"
    echo "------------------------------------------------------------------------------------------"
    echo
    NXCALS_JAVA_VERSION=${NXCALS_JAVA_VERSION_NEW}
else
    echo
    echo "No Update for the NXCALS Java package. It is still at version ${NXCALS_JAVA_VERSION}."
    echo
fi


# Check if Python NXCALS package got an update
# --------------------------------------------
if [[ ${NXCALS_PYTHON_VERSION} != ${NXCALS_PYTHON_VERSION_NEW} ]]; then
    
    # Clean previous installation if necessary
    rm -rf ${TARGET_VIEW}/nxcals/${NXCALS_PYTHON}
    rm -rf ${TARGET_VIEW}/lib/${PYTHON_VER}/site-packages/cern
    rm -rf ${TARGET_VIEW}/lib/${PYTHON_VER}/site-packages/tests
    rm -rf ${TARGET_VIEW}/lib/${PYTHON_VER}/site-packages/nxcals*-info
    
    cd ${TARGET_VIEW}/nxcals
    
    # Copy files from Jenkins workspace to local folder before extraction
    cp ${WORKSPACE}/${NXCALS_PYTHON}.zip ${TARGET_VIEW}/nxcals
    
    # Unpack package and remove zip file
    unzip ${NXCALS_PYTHON}.zip -d ${NXCALS_PYTHON}
    rm -rf ${NXCALS_PYTHON}.zip

    # Create cached bytecode to improve performance using the correct Python version from the view
    # This uses the same release but as a platform the SLC6 platform is used since that is the current platform of the release manager machine.
    # To avoid interference, the command is started inside a subshell
    PYHON3_VIEW=${CVMFS_PATH}/LCG_${LCG_RELEASE}/x86_64-slc6-gcc7-opt
    if [ ! -d "${PYHON3_VIEW}" ]
    then
        # Use LCG_94python3 as backup if SLC6 view doesn't exist
        echo "Fallback to 'LCG_94python3' for python3 compilation"
        PYHON3_VIEW=${CVMFS_PATH}/LCG_94python3/x86_64-slc6-gcc7-opt
    fi
    (
        source ${PYHON3_VIEW}/setup.sh
        cd ${NXCALS_PYTHON}
        python -m compileall .
    )
    # Create symbolic links
    for filename in ${TARGET_VIEW}/nxcals/${NXCALS_PYTHON}/*; do
        ln -s ${filename} ${TARGET_VIEW}/lib/${PYTHON_VER}/site-packages
        ls -hdl ${TARGET_VIEW}/lib/${PYTHON_VER}/site-packages/$(basename ${filename})
    done
    
    cd ${TARGET_VIEW}
    GENERATION_PATTERN="#   Generated: "
    MODIFICATION_PATTERN="#   Modified for NXCALS: "
    MODIFICATION_MESSAGE="${MODIFICATION_PATTERN}$(date --universal)"
    
    # Modify setup.sh: Add modification time stamp and set PYTHON_PATH if necessary
    if grep -q "${MODIFICATION_PATTERN}" setup.sh
    then
        sed -i "s/${MODIFICATION_PATTERN}.*/${MODIFICATION_MESSAGE}/" setup.sh
    else
        sed -i "/${GENERATION_PATTERN}.*/a ${MODIFICATION_MESSAGE}" setup.sh
    fi
    
    # Modify setup.csh: Add modification time stamp and set PYTHON_PATH if necessary
    if grep -q "${MODIFICATION_PATTERN}" setup.csh
    then
        sed -i "s/${MODIFICATION_PATTERN}.*/${MODIFICATION_MESSAGE}/" setup.csh
    else
        sed -i "/${GENERATION_PATTERN}.*/a ${MODIFICATION_MESSAGE}" setup.csh
    fi
    
    # Update version in the version file
    echo "NXCALS_JAVA_VERSION=${NXCALS_JAVA_VERSION}" > ${OLD_VERSION_FILE}
    echo "NXCALS_PYTHON_VERSION=${NXCALS_PYTHON_VERSION_NEW}" >> ${OLD_VERSION_FILE}
    
    echo
    echo "------------------------------------------------------------------------------------------"
    echo "Updated NXCALS Python package from version ${NXCALS_PYTHON_VERSION} to ${NXCALS_PYTHON_VERSION_NEW}"
    echo "------------------------------------------------------------------------------------------"
    echo
else
    echo
    echo "No Update for the NXCALS Python package. It is still at version ${NXCALS_PYTHON_VERSION}."
    echo
fi
