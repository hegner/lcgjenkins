#!/usr/bin/python
import sys, glob, os
import shutil
import subprocess
import Tools as tools

# Removal of nightly builds from CVMFS

##########################
if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()

    (options, args) = parser.parse_args()
    if len(args)!=2:
        print "Please provide a slot name and day of the week to clean!"
        sys.exit(-2)
    slot = args[0]
    day_option = args[1]

    if day_option=="tomorrow":
        day = tools.tomorrow()    
    else:
        day = day_option 

    BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

    print "Cleaning nightly builds of slot '%s' for day %s" %(slot,day) 

    rootdir = tools.pathJoin(BASE,slot,day)

    print 'Removing the isDone files'
    command = 'rm %s/isDone*' %(rootdir)
    tools.executeAndCheckCommand(command,"isDone files removed","isDone files not removed")

    print 'Removing the LCG_externals files'
    command = 'rm %s/LCG_externals*' %(rootdir)
    tools.executeAndCheckCommand(command,"LCG_externals files removed","LCG_externals files not removed")

    print 'Removing the LCG_generators files'
    command = 'rm %s/LCG_generators*' %(rootdir)
    tools.executeAndCheckCommand(command,"LCG_generators files removed","LCG_generators files not removed")

    dirs = glob.glob(os.path.join(rootdir, '*/*'))
    files = glob.glob(os.path.join(rootdir, '*/.*'))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*')))
    dirs.extend(glob.glob(os.path.join(rootdir, '*/*/*/*')))

    for f in files:
        os.remove(f)

    for d in dirs:
        if os.path.islink(d):
            print 'Unlinking directory %s' % (d)
            os.unlink(d)
        else:
            print 'Removing  directory %s' % (d)
            shutil.rmtree(d)

    command = 'rmdir %s/*' %(rootdir)        
    tools.executeAndCheckCommand(command,"empty package dirs removed","empty package dirs not removed")
